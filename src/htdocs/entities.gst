<!ENTITY gst-branch-stable "1.16">
<!ENTITY gst-version-stable "1.16.2">
<!ENTITY gst-plugins-base-version-stable "1.16.2">
<!ENTITY gst-plugins-good-version-stable "1.16.2">
<!ENTITY gst-plugins-ugly-version-stable "1.16.2">
<!ENTITY gst-plugins-bad-version-stable "1.16.2">

<!ENTITY gst-ffmpeg-version-stable "obsolete">
<!ENTITY gst-libav-version-stable "1.16.2">
<!ENTITY gst-openmax-version-stable "obsolete">
<!ENTITY gst-python-version-stable "1.16.2">
<!ENTITY gstreamer-sharp-version-stable "1.16.0">
<!ENTITY gst-plugins-gl-version-stable "obsolete">
<!ENTITY gst-rtsp-server-version-stable "1.16.2">
<!ENTITY gst-streaming-server-version-stable "git master">
<!ENTITY gnonlin-version-stable "obsolete">
<!ENTITY gst-editing-services-version-stable "1.16.2">
<!ENTITY qt-gstreamer-version-stable "1.2.0">
<!ENTITY gst-android-version-stable "obsolete">
<!ENTITY gst-omx-version-stable "1.16.2">
<!ENTITY gst-vaapi-version-stable "1.16.2">
<!ENTITY gst-validate-stable "1.16.2">

<!ENTITY gst-branch-devel "git master">
<!ENTITY gst-version-devel "1.17.2">
<!ENTITY gst-plugins-base-version-devel "1.17.2">
<!ENTITY gst-plugins-good-version-devel "1.17.2">
<!ENTITY gst-plugins-ugly-version-devel "1.17.2">
<!ENTITY gst-plugins-bad-version-devel "1.17.2">
<!ENTITY gst-plugins-gl-version-devel "git master">
<!ENTITY gst-rtsp-server-version-devel "1.17.2">
<!ENTITY gst-streaming-server-version-devel "git master">
<!ENTITY qt-gstreamer-version-devel "N/A">
<!ENTITY gst-android-version-devel "git master">
<!ENTITY gst-omx-version-devel "1.17.2">
<!ENTITY gst-vaapi-version-devel "1.17.2">

<!ENTITY gst-validate-devel "git master">
<!ENTITY gst-python-version-devel "1.17.2">
<!ENTITY gnonlin-version-devel "git master">
<!ENTITY gst-editing-services-version-devel "1.17.2">

<!ENTITY gst-ffmpeg-version-devel "git master">
<!ENTITY gst-libav-version-devel "1.17.2">
<!ENTITY gst-openmax-version-devel "git master">
<!ENTITY gstreamer-sharp-version-devel "1.17.2">

<!ENTITY orc-version-stable "0.4.31">
<!ENTITY orc-version-devel "git master">

<!ENTITY gst-bug-report "http://bugzilla.gnome.org/enter_bug.cgi?product=GStreamer">
<!ENTITY gst-repo-http "https://gitlab.freedesktop.org/gstreamer/">
<!ENTITY realsite "https://gstreamer.freedesktop.org">
<!ENTITY nbsp "&#x00A0;">
